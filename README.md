# Wine Shop Website

Welcome to the Wine Shop project! This repository contains the source code and assets for a simple online wine shop built with HTML, CSS and Javascript.

## Introduction

The Wine Shop Website is a web-based platform that allows users to explore a curated selection of wines and make purchases online. It provides a user-friendly interface for browsing wines, viewing details, and adding them to the cart.

## Features

- Browse a variety of wines by category and region.
- View detailed information about each wine, including name, description, price, and origin.
- Add wines to the shopping cart and review the cart contents.
- Proceed to checkout to complete a purchase (Note: This is a simulated checkout process and does not involve real payments).
- Responsive design for a seamless experience on both desktop and mobile devices.

## Usage
You can use this project as a template for building your own online wine shop or e-commerce website. Feel free to customize it to suit your needs. Here are some potential ways to extend the project:

Integrate a backend server to handle real purchases and user accounts.
Add more wine categories and products to expand your product catalog.
Implement user authentication and registration.
Enhance the user interface with additional features such as search and filtering.

## Technologies
Programming Languages: The project uses HTML, CSS, and JavaScript (Next.js) for front-end development. For server-side and data management, Node.js is used.

Database: NoSQL is used (e.g., MongoDB).

Frameworks and Libraries: Front-end frameworks such as React or Angular can be used to create a modern and dynamic interface. CSS is used for styling.

Source Code Management: It uses a version control system, such as Git or GitLab, to manage and collaborate on the project's source code.

Authentification: NextAuth.js for securing the admin account and also for log in.
## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.upt.ro/anca.lazar/proiectpw.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.upt.ro/anca.lazar/proiectpw/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)


