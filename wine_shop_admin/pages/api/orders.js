// api/orders.js
import { mongooseConnect } from '@/lib/mongoose';
import { Order } from '../../models/Orders';

export default async function handler(req, res) {
  await mongooseConnect();

  if (req.method === 'GET') {
    const orders = await Order.find().sort({ createdAt: -1 });
    res.json(orders);
  } else if (req.method === 'DELETE') {
    try {
      const { orderId } = req.body; // Extract the order ID from the request body
      await Order.deleteOne({ _id: orderId });
      res.status(204).end();
    } catch (error) {
      console.error('Error deleting order:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  } else {
    res.status(405).end();
  }
}

