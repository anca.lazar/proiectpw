import NextAuth from "next-auth"
import GoogleProvider from "next-auth/providers/google"
import { MongoDBAdapter } from "@next-auth/mongodb-adapter"
import clientPromise from "../../../lib/mongodb"

const adminEmail=['ancalazar333@gmail.com'];
const authOptions={
  providers:[
    GoogleProvider({
      clientId:process.env.GOOGLE_ID,
      clientSecret:process.env.GOOGLE_SECRET
    }),
  ],

  adapter: MongoDBAdapter(clientPromise),
  callbacks:{
    session:({session, token, user}) => {
      if(adminEmail.includes(session?.user?.email)){
        return session;
      } else{
        return false;
      }
      
      
    },
    async signOut({ baseUrl }) {

      return '/admin';
    },
  },
};

export default NextAuth(authOptions);
export async function isAdminRequest(req,res){
  const session=await getServerSession(req, res, authOptions);
  if(adminEmail.includes(session?.user?.email)){
    throw 'The account is not an admin account';
  }
  
}

