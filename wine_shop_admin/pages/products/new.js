import Layout from "@/javascriptScripts/Layout";
import newstyle from "../../css/newstyle.module.css"
import { useState } from "react";
import axios from "axios";
import { redirect } from "next/dist/server/api-utils";
import { useRouter } from "next/router";
import ProductForm from "@/javascriptScripts/FormProduct";

export default function NewProduct() {
    return (
        <Layout>
            <h1 className={newstyle.hStyle}><b>Add new product</b></h1>
            <ProductForm />
        </Layout>
    );
}