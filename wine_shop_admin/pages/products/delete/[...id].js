import Layout from "@/javascriptScripts/Layout";
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import modalstyle from '../../../css/modal.module.css';
import axios from "axios";

export default function DeleteProduct() {
    const router = useRouter();
    const [showModal, setShowModal] = useState(true);
    const { id } = router.query;
    const [productInfo, setProductInfo] = useState({ title: '' }); // Provide a default value

    useEffect(() => {
        if (!id) {
            return;
        }
        axios.get('/api/products?id=' + id).then(response => {
            setProductInfo(response.data);
        });
    }, [id]);

    function goBack() {
        router.push('/products');
    }

    async function handleDelete() {
        await axios.delete('/api/products?id='+id)
        
        setShowModal(false);
        goBack();
    }

    return (
        <div className={modalstyle.bgimg}>
            <Layout>
                {showModal && (
                    <div className={modalstyle.modal}>
                        <div className={modalstyle.modalContent}>
                            <p className={modalstyle.pModal}>
                                Do you really want to delete {productInfo.title}?
                            </p>
                            <button className={modalstyle.buttonModal} onClick={handleDelete} >Yes</button>
                            <button className={modalstyle.buttonModal} onClick={() => { setShowModal(false); goBack(); }}>No</button>
                        </div>
                    </div>
                )}
            </Layout>
        </div>
    );
}
