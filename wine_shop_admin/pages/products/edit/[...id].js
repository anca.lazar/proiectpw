import Layout from "@/javascriptScripts/Layout";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import ProductForm from "@/javascriptScripts/FormProduct";
import editstyle from '../../../css/editstyle.module.css';

export default function EditProductPage() {
    const [productInfo, setProductInfo] = useState(null);
    const router = useRouter();
    const { id } = router.query;

    useEffect(() => {
        if (id) {
            axios.get('/api/products?id=' + id)
                .then(response => {
                    setProductInfo(response.data);
                })
                
        }
    }, [id]);

    return (
        <Layout>
            <h1 className={editstyle.hStyle}><b>Edit product</b></h1>
            {productInfo ? (
                <ProductForm {...productInfo} />
            ) : (
                
                <p>Loading...</p>
            )}
        </Layout>
    );
}
