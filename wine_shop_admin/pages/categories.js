import Layout from "@/javascriptScripts/Layout";
import editstyle from '../css/editstyle.module.css';
import categorystyle from '../css/category.module.css';
import { useEffect, useState } from "react";
import axios from "axios";
import Link from "next/link";
import React from 'react'
import { withSwal } from "react-sweetalert2";

function Categories({ swal }) {
    const [editedCategory, setEditedCategory] = useState(null);
    const [name, setName] = useState('');
    const [parentCategory, setParentCategory] = useState('');
    const [categories, setCategories] = useState([]);
    const [properties, setProperties] = useState([]);

    useEffect(() => {
        fetchCategories();
    }, []);

    async function fetchCategories() {
        axios.get('/api/categories').then(result => {
            setCategories(result.data);
        });
    }

    function deleteCategory(category){
        swal.fire({
          title: 'Are you sure?',
          showCancelButton: true,
          cancelButtonText: 'Cancel',
          confirmButtonText: 'Yes',
          confirmButtonColor: '#800000',
          
        }).then(async result => {
          if (result.isConfirmed) {
            const {_id} = category;
            await axios.delete('/api/categories?_id='+_id);
            fetchCategories();
          }
        });
      }

    function addProperty() {
        setProperties(prev => {
            return [...prev, { name: '', values: '' }];
        });
    }

    function handelPropertyName(index,property, newName){
        setProperties(prev => {
            const properties = [...prev];
            properties[index].name=newName;
            return properties;
        });
    }

    function handelPropertyValues(index,property, newValues){
        setProperties(prev => {
            const properties = [...prev];
            properties[index].values=newValues;
            return properties;
        });
    }

    async function saveCategory(ev){
        ev.preventDefault();
        const data = {
          name,
          parentCategory,
          properties:properties.map(p => ({
            name:p.name,
            values:p.values.split(','),
          })),
        };
        if (editedCategory) {
          data._id = editedCategory._id;
          await axios.put('/api/categories', data);
          setEditedCategory(null);
        } else {
          await axios.post('/api/categories', data);
        }
        setName('');
        setParentCategory('');
        setProperties([]);
        fetchCategories();
      }

    function getParentCategoryName(categoryId) {
        if (categoryId === undefined) {
            return 'No parent category';
        }
    
        const parentCategory = categories.find(category => category._id === categoryId);
        return parentCategory ? parentCategory.name : 'No parent category';
    }
    
    function editCategory(category){
        setEditedCategory(category);
        setName(category.name);
        setParentCategory(category.parent?._id);
        setProperties(
          category.properties.map(({name,values}) => ({
          name,
          values:values.join(',')
        }))
        );
      }

    function removeProperty(index){
        setProperties(prev =>{
            return [...prev].filter((prop,propIndx) => {
                return propIndx!=index;
            });
             
        });
    }

    return (
        <Layout>
            <div className={categorystyle.center}>
                <h1 className={categorystyle.hStyle}><b>Categories</b></h1>
                <label className={categorystyle.label}>
                    {editedCategory ? `Edit category ${editedCategory.name}` : 'Create new category'}
                </label>


                <form onSubmit={saveCategory} className={categorystyle.form}>
                    <div className="flex gap-1">
                        <input type="text" placeholder={'Category name'} className={categorystyle.input} onChange={ev => setName(ev.target.value)} value={name} />
                        <select className={categorystyle.select} value={parentCategory} onChange={ev => setParentCategory(ev.target.value)}>
                            <option value="">
                                <div className={categorystyle.textOption}>No parent category</div>
                            </option>
                            {categories.length > 0 && categories.map(category => (
                                <option key={category._id} value={category._id}>{category.name}</option>
                            ))}
                        </select>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }} className="gap-7">
                        <label>Properties</label>
                        <button className={categorystyle.button} type="button" onClick={addProperty}>
                            Add New Property
                        </button>
                    </div>
                    <div >
                        {properties.length > 0 &&
                            properties.map((property, index) => (
                                <div className="flex gap-2 mb-8">
                                    <input type="text" value={property.name} onChange={ev => handelPropertyName(index, property, ev.target.value)} placeholder="Property name" />
                                    <input type="text" value={property.values} onChange={ev => handelPropertyValues(index, property, ev.target.value)} placeholder="Values - separate by comma" />
                                    <button onClick={()=>removeProperty(index)} className={categorystyle.button} style={{marginTop: '0px', paddingRight:1,paddingLeft:1, marginLeft:'15px' }}>Delete</button>
                                </div>
                            ))}
                    </div>


                    <button type={'submit'} className={categorystyle.button}>Save</button>
                </form>
                <table className={categorystyle.tablestyle}>
                    <thead>
                        <tr>
                            <td>
                                Category name
                            </td>
                            <td>
                                Parent category
                            </td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.length > 0 && categories.map(category => (
                            <tr key={category._id}>
                                <td>
                                    {category.name}
                                </td>
                                <td>
                                    {/* {getParentCategoryName(category.parent)} */}
                                    {category?.parent?.name}
                                </td>
                                <td className="flex gap-4">
                                    <div className="bg-black rounded-lg flex mb-4 mt-3 p-1">
                                        <button className="ml-2 flex" onClick={() => editCategory(category)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5 flex" width={10} height={10}>
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                                            </svg>
                                            Edit
                                        </button >
                                    </div>
                                    <div className="bg-black rounded-lg flex p-1 mb-4 mt-3">
                                        <button className="ml-2 flex" onClick={() => deleteCategory(category)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5 flex" width={10} height={10}>
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                            </svg>
                                            Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </Layout>
    );
}

export default withSwal(({ swal }, ref) => (
    <Categories swal={swal} />
));
