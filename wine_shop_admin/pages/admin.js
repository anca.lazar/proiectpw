
import styles from '../css/styles.module.css';
import { useSession, signIn, signOut } from "next-auth/react";
import Nav from '@/javascriptScripts/navigation';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

export default function Admin() {
  const { data: session } = useSession();
  const router = useRouter();


  if (!session) {
    return (
      <div className={styles.contentContainer2}>
        <div className="text-center w-full">
          <button onClick={() => signIn('google')} className={styles.btnLogin}>
            Login with Google
          </button>
        </div>
      </div>
    );
  } else {   
      router.push('/');

    return null;
  }
}
