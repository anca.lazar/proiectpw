import Layout from "@/javascriptScripts/Layout";
import { useEffect, useState } from "react";
import axios from "axios";
import style from '../css/category.module.css'
import { withSwal } from "react-sweetalert2";

export default function OrdersPage({ swal }) {
    const [orders, setOrders] = useState([]);
    useEffect(() => {
        axios.get('/api/orders').then(response => {
            setOrders(response.data);
        });
    }, []);

    const customStyles = {
        backgroundColor: 'black',
        color: 'wheat',
        textAlign: 'center',

    };

    const orderStyles = {
        fontSize: '50px',
        paddingLeft: '40rem',
        marginBottom: '50px'


    };



    function removeOrder(orderId, index) {
        axios.delete('/api/orders', {
          data: { orderId }, // Send the order ID in the request body
        }).then((response) => {
          if (response.status === 204) {
            // Order was successfully deleted on the server
            setOrders((prevOrders) => {
              const updatedOrders = [...prevOrders];
              updatedOrders.splice(index, 1);
              return updatedOrders;
            });
          }
        });
      }
      


    return (
        <Layout>
            <h1 style={orderStyles}><b>Orders</b></h1>
            <table className="basic" style={customStyles}>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Recipient</th>
                        <th>Products</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {orders.length > 0 && orders.map((order,index) => (
                        <tr key={order._id}>
                            <td>{(new Date(order.createdAt)).toLocaleString()}
                            </td>

                            <td>
                                {order.name} {order.email}<br />
                                {order.city} {order.postalCode} {order.country}<br />
                                {order.streetAddress}
                            </td>
                            <td>
                                {order.line_items.map(l => (
                                    <>
                                        {l.price_data?.product_data.name} x
                                        {l.quantity}<br />
                                    </>
                                ))}
                            </td>
                            <td>
                                <button
                                    className={style.button}
                                    onClick={() => removeOrder(order._id, index)}
                                >
                                    Delete Order
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </Layout>
    );
}