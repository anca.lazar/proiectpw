import Layout from '@/javascriptScripts/Layout';
import {useSession} from "next-auth/react";
import style from '../css/home.module.css'


export default function Home() {
  const {data:session}=useSession();
  if(!session){
    return;
  }
  return <Layout>
    <header>
    <div className='flex justify-between mr-1 ml-10'>
      <h2>
    Hello, <b>{session.user?.name}</b>
    </h2>
    <div className='flex bg-red-900 gap-1 rounded-lg overflow-hidden'>
    <img className="w-6 h-6"src={session?.user?.image} alt=""/>
    <span className=' px-2'>
    {session?.user?.name}
    </span>
    </div>
    </div>
    </header>
    <body className={style.body}>
    <div class={style.welcome_container}>
        <h1 className={style.h1}>Welcome to the Admin Dashboard</h1>
        <p className={style.p}>Hello, wine enthusiast and administrator of Bear Garden! We are delighted to have you here in the heart of our wine shop's operations. As an admin, you have the power to manage, curate, and oversee the world of wines we offer to our customers. Here, you can update our wine catalog, track orders, and keep a close eye on our wine inventory.</p>
        <h2 className={style.h2}>What You Can Do:</h2>
        <ul className={style.ul}>
            <li className={style.li}>Manage Wine Catalog: Add new wines, update existing ones, and ensure our wine selection is always up-to-date.</li>
            <li className={style.li}>Track Orders: Stay informed about the orders that our customers have placed, and ensure they are processed smoothly.</li>
            <li className={style.li}>Inventory Management: Keep an eye on our wine stock, and make sure we never run out of our customers' favorite bottles.</li>
           
        </ul>
        <p className={style.p}>At Bear Garden, we're passionate about wine, and we know you are too. Our mission is to provide the best selection of wines and a seamless shopping experience for our customers. We're here to support you every step of the way.</p>
        <p className={style.p}>If you have any questions, need assistance, or have ideas to enhance our wine shop, don't hesitate to reach out to our team. Cheers to a successful and enjoyable journey in managing our wine shop!</p>
        <p className={style.p}>Sip, savor, and succeed!</p>
    </div>
</body>
  </Layout>

}
