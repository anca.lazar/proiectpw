import Layout from "./Layout";
import newstyle from '../css/newstyle.module.css';
import { useEffect, useState } from "react";
import axios from "axios";
import { redirect } from "next/dist/server/api-utils";
import { useRouter } from "next/router";
import Spinner from "./Spinner";

export default function ProductForm({
  _id,
  title: currentTitle,
  description: currentDescription,
  price: currentPrice,
  images: currentImages,
  category:assignedCategory,
  properties:assignedProperties,
}) {
  const [title, setTitle] = useState(currentTitle || "");
  const [description, setDescription] = useState(currentDescription || "");
  const [price, setPrice] = useState(currentPrice || "");
  const [images, setImages] = useState(currentImages || []);
  const [isUploading, setIsUploading] = useState(false);
  const [goBack, setGoBack] = useState(false);
  const [categories, setCategories] = useState([]);
  const[category, setCategory]=useState(assignedCategory || '');
  const [productProp, setProductProp]=useState(assignedProperties||{});
  const router = useRouter();

  useEffect(() => {
    axios.get('/api/categories').then(result => {
      setCategories(result.data);
    })
  }, []);

  async function saveProduct(ev) {
    ev.preventDefault();
    const data = { title, description, price, images, category, properties:productProp, };
    if (_id) {
      await axios.put('/api/products', { ...data, _id });
    } else {
      await axios.post("/api/products", data);
    }
    setGoBack(true);
  }

  if (goBack) {
    router.push('/products');
  }

  async function uploadImage(ev) {
    const files = ev.target?.files;
    if (files?.length > 0) {
      setIsUploading(true);
      const data = new FormData();
      for (const file of files) {
        data.append('file', file);
      }

      const res = await axios.post('/api/upload', data);

      setImages((oldImages) => {
        return [...oldImages, ...res.data.links];
      });

      setIsUploading(false);
    }
  }

  const propertiesList = [];

  if (categories.length > 0 && category) {
    let catInfo = categories.find(({ _id }) => _id === category);
  
    if (catInfo) { 
      propertiesList.push(...catInfo.properties);
    
      while (catInfo?.parent?._id) {
        const parentCat = categories.find(({ _id }) => _id === catInfo?.parent?._id);
  
        if (parentCat) { 
          propertiesList.push(...parentCat.properties);
          catInfo = parentCat;
        } else {
          break; 
        }
      }
    }
  }

  function changeProductProp(prop, value){
    setProductProp(prev =>{
      const newProductProp={...prev};
      newProductProp[prop] =value;
      return newProductProp;
    })
  }
  
  return (
    <form onSubmit={saveProduct}>
      <div className={newstyle.bodyStyle}>
        <label>Product Name</label>
        <input
          className={newstyle.inputStyle}
          type="text"
          placeholder="Name of product"
          value={title}
          onChange={(ev) => setTitle(ev.target.value)}
        />
        <label>Category</label>
        <select className="ml-4 bg-black" value={category} onChange={ev=>setCategory(ev.target.value)}>
          <option>Uncategorized</option>
          {categories.length > 0 &&
            categories.map((c) => (
              <option value={c._id} key={c._id}>
                {c.name}
              </option>
            ))}
        </select>
        {propertiesList.length>0  &&propertiesList.map(
          p=>(
            <div className="bg-black">
            <div className="ml-4 flex gap-2 ">
              {p.name}
            <select value={productProp[p.name]} type="text" className="bg-black" onChange={(ev)=>{changeProductProp(p.name,ev.target.value)}}>
              {p.values.map(v=>(
                <option value={v}>{v}</option> 
              ))}
              
              </select>
            </div>
            </div>
            
          )
        )}

        <label>Photos</label>
        <div className="flex flex-wrap gap-2">
          {images?.length > 0 &&
            images.map((link) => (
              <div key={link} className="h-64 w-52 ">
                <img
                  src={link}
                  height={160}
                  width={160}
                  className="rounded-lg flex ml-4"
                />
              </div>
            ))}
          {isUploading && (
            <div className="p-6 bg-gray-700 flex items-center justify-center w-52 h-52 rounded-lg">
              <Spinner width={40} height={40} />
            </div>
          )}
          <div class={newstyle.buttonContainer}>
            <label className={newstyle.buttonUpload}>
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.5"
                  stroke="currentColor"
                  className="w-4 h-4 flex ml-3"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M9 8.25H7.5a2.25 2.25 0 00-2.25 2.25v9a2.25 2.25 0 002.25 2.25h9a2.25 2.25 0 002.25-2.25v-9a2.25 2.25 0 00-2.25-2.25H15m0-3l-3-3m0 0l-3 3m3-3V15"
                  />
                </svg>
                <div>Upload</div>
              </div>
              <input
                type="file"
                onChange={uploadImage}
                className={newstyle.inputHidden}
              />
            </label>
          </div>
        </div>
        <label>Description</label>
        <textarea
          className={newstyle.textareaStyle}
          placeholder="Description of product"
          value={description}
          onChange={(ev) => setDescription(ev.target.value)}
        ></textarea>
        <label>Price</label>
        <input
          className={newstyle.inputStyle}
          type="number"
          placeholder="Price"
          min={0}
          value={price}
          onChange={(ev) => setPrice(ev.target.value)}
        />
        <button type="submit" className={newstyle.buttonSave}>
          Save
        </button>
      </div>
    </form>
  );
}
