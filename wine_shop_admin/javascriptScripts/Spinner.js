import { RingLoader } from "react-spinners";

export default function Spinner() {
    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '200px', 
            border: '1px',
            width:'150px'
        }}>
            <RingLoader color={'#800000'} />
        </div>
    );
}
