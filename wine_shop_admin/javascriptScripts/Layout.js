import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '../css/styles.module.css';
import { useSession, signIn, signOut } from "next-auth/react"
import Nav from '@/javascriptScripts/navigation';

const inter = Inter({ subsets: ['latin'] })

export default function Layout({children}) {
  const { data: session } = useSession();
  if (!session) {
    return (<div className={styles.contentContainer2}>
      <div className="text-center w-full">
        <button onClick={()=>signIn('google')} className={styles.btnLogin}>Login with Google</button>
        
      </div>
    </div>
    )
  }

  return (
    <div className='min-h-screen'>
      <Nav></Nav>
    <div className='mr-2 mt-2'>{children}</div>
    </div>
  )

}