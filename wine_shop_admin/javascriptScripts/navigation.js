import Image from 'next/image';
import imagePath from '../images/logo.jpg'
import stylesheet from '../css/styles.module.css'
import Link from 'next/link';
import {signOut} from "next-auth/react"

export default function Nav() {
    return (

        <div className={stylesheet.generalSettings}>
            <header className='p-4' style={{ fontWeight: 'bold' }}>

                <ul className={stylesheet.navMenu}>
                    <li style={{ marginTop: '5px', marginLeft: '10px' }}>
                        <div className={stylesheet.myImage}>
                            <Image
                                src={imagePath}
                                width={50}
                                height={50}

                            />

                        </div>
                    </li>



                    <li>
                        <Link href={'/'} className={stylesheet.linkEdit} >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className={stylesheet.svgIcon}>
                                <path strokeLinecap="round" strokeLinejoin="round" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
                            </svg>

                            <span  >
                                Admin
                            </span>
                        </Link>
                    </li>


                    <li>
                        <Link href={'/orders'} className={stylesheet.linkEdit}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className={stylesheet.svgIcon}>
                                <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" />
                            </svg>


                            Orders
                        </Link>
                    </li>
                    <li>
                        <Link href={'/products'} className={stylesheet.linkEdit}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className={stylesheet.svgIcon} >
                                <path strokeLinecap="round" strokeLinejoin="round" d="M20.25 7.5l-.625 10.632a2.25 2.25 0 01-2.247 2.118H6.622a2.25 2.25 0 01-2.247-2.118L3.75 7.5M10 11.25h4M3.375 7.5h17.25c.621 0 1.125-.504 1.125-1.125v-1.5c0-.621-.504-1.125-1.125-1.125H3.375c-.621 0-1.125.504-1.125 1.125v1.5c0 .621.504 1.125 1.125 1.125z" />
                            </svg>

                            Products
                        </Link>
                    </li>
                    <li>
                        <Link href={'/categories'} className={stylesheet.linkEdit}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 12h16.5m-16.5 3.75h16.5M3.75 19.5h16.5M5.625 4.5h12.75a1.875 1.875 0 010 3.75H5.625a1.875 1.875 0 010-3.75z" />
                            </svg>


                            Categories
                        </Link>
                    </li>
                    
                    <li>
                        <button className="flex" onClick={() => signOut({ callbackUrl: '/admin' })}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
                            </svg>


                            Logout
                        </button>
                    </li>
                </ul>
            </header>
        </div>
    );
}
