import Link from "next/link";

export default function Header(){
    return(
        <header>
            <Link href={'/'}>WineShop</Link>
            <nav>
                <Link href={'/'}>Home</Link>
                <Link href={'/products'}>All product</Link>
                <Link href={'/categories'}>Categories</Link>
                <Link href={'/account'}>Account</Link>
                <Link>Cart</Link>
            </nav>
        </header>
    );
}